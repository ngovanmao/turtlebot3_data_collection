#!/usr/bin/env python

import rospy
import os
from geometry_msgs.msg import Twist
from nav_msgs.msg import Odometry
from sensor_msgs.msg import Imu
import datetime
import argparse

class DataCollection():
    def __init__(self, args):
        path = 'dataset/{}_{}'.format(args.mode, args.scenario)#normal_pentagon'
        if not os.path.isdir(path):
            os.makedirs(path)
        self.cmd_vel_file = open(os.path.join(path, 'cmd_vel.csv'), "w+")
        self.cmd_vel_file.write(\
        "stamp_secs,stamp_nsecs,linear_x,linear_y,linear_z,"
        "angular_x,angular_y,angular_z\n")

        self.odom_file = open(os.path.join(path, 'odometry.csv'), "w+")
        # no covariance, no frame_id, no child_frame_id
        self.odom_file.write(\
        "header_seq,h_stamp_secs,h_stamp_nsecs,"
        "pose_pose_position_x,pose_pose_position_y,pose_pose_position_z,"
        "pose_pose_orientation_x,pose_pose_orientation_y,pose_pose_orientation_z,"
        "pose_pose_orientation_w,twist_twist_linear_x,twist_twist_linear_y,"
        "twist_twist_linear_z,twist_twist_angular_x,twist_twist_angular_y,"
        "twist_twist_angular_z\n")

        self.imu_file = open(os.path.join(path, 'imu.csv'), "w+")
        self.imu_file.write(\
        "header_seq,h_stamp_secs,h_stamp_nsecs,"
        "orientation_x,orientation_y,orientation_z,"
        "angular_x,angular_y,angular_z,"
        "linear_accel_x,linear_accel_y,linear_accel_z\n")

        self.hack_cmd_file = open(os.path.join(path, 'hack_cmd.csv'), "w+")
        self.hack_cmd_file.write("stamp_secs,stamp_nsecs,linear_x,linear_y,"
        "linear_z,angular_x,angular_y,angular_z\n")

        self.hack_odom_file = open(os.path.join(path, 'hack_odom.csv'), "w+")
        self.hack_odom_file.write("stamp_secs,stamp_nsecs,linear_x,linear_y,"
        "linear_z,angular_x,angular_y,angular_z\n")


        self.cmd_vel_sub = rospy.Subscriber('cmd_vel', Twist, self.get_cmd,
            queue_size = 10)
        self.odom_sub = rospy.Subscriber('odom', Odometry, self.get_odom,
            queue_size = 10)
        self.imu_sub = rospy.Subscriber('imu', Imu, self.get_imu,
            queue_size = 10)
        self.hack_cmd_sub = rospy.Subscriber('hack_cmd', Twist,
            self.get_hack_cmd, queue_size = 10)
        self.hack_odom_sub = rospy.Subscriber('hack_odom', Twist,
            self.get_hack_odom, queue_size = 10)

        rospy.loginfo("Logging cmd_vel, imu, and odometry INIT")
        rospy.loginfo("Start at: {}".format(
            datetime.datetime.now().strftime("%Y-%m-%d %H:%M")))
        self.data_collection()

    def get_cmd(self, recv_cmd):
        """
        rosmsg show geometry_msgs/Twist
            geometry_msgs/Vector3 linear
              float64 x
              float64 y
              float64 z
            geometry_msgs/Vector3 angular
              float64 x
              float64 y
              float64 z
        """
        rospy.logdebug("Get cmd_vel")
        now =  rospy.get_rostime()
        self.cmd_vel_file.write("{},{},{},{},{},{},{},{}\n".format(
            now.secs,now.nsecs,
            recv_cmd.linear.x,recv_cmd.linear.y,recv_cmd.linear.z,
            recv_cmd.angular.x,recv_cmd.angular.y,recv_cmd.angular.z))
        # Store cmd_vel into database

    def get_hack_cmd(self, recv_cmd):
        rospy.logdebug("Get hack_cmd_vel")
        now =  rospy.get_rostime()
        self.hack_cmd_file.write("{},{},{},{},{},{},{},{}\n".format(
            now.secs,now.nsecs,
            recv_cmd.linear.x,recv_cmd.linear.y,recv_cmd.linear.z,
            recv_cmd.angular.x,recv_cmd.angular.y,recv_cmd.angular.z))

    def get_odom(self, recv_odom):
        """ rosmsg info nav_msgs/Odometry
            std_msgs/Header header
              uint32 seq
              time stamp
              [string frame_id]
            [string child_frame_id]
            geometry_msgs/PoseWithCovariance pose
              geometry_msgs/Pose pose
                geometry_msgs/Point position
                  float64 x
                  float64 y
                  float64 z
                geometry_msgs/Quaternion orientation
                  float64 x
                  float64 y
                  float64 z
                  float64 w
              [float64[36] covariance]
            geometry_msgs/TwistWithCovariance twist
              geometry_msgs/Twist twist
                geometry_msgs/Vector3 linear
                  float64 x
                  float64 y
                  float64 z
                geometry_msgs/Vector3 angular
                  float64 x
                  float64 y
                  float64 z
              [float64[36] covariance]
        """
        rospy.logdebug("Get odom")
        self.odom_file.write(
            "{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{}\n".format(
            recv_odom.header.seq, recv_odom.header.stamp.secs,
            recv_odom.header.stamp.nsecs,
            recv_odom.pose.pose.position.x,recv_odom.pose.pose.position.y,
            recv_odom.pose.pose.position.z,
            recv_odom.pose.pose.orientation.x,recv_odom.pose.pose.orientation.x,
            recv_odom.pose.pose.orientation.z,recv_odom.pose.pose.orientation.w,
            recv_odom.twist.twist.linear.x,recv_odom.twist.twist.linear.y,
            recv_odom.twist.twist.linear.z,
            recv_odom.twist.twist.angular.x,recv_odom.twist.twist.angular.y,
            recv_odom.twist.twist.angular.z
            )
        )


    def get_hack_odom(self, recv_odom):
        rospy.logdebug("Get hack_odom")
        now =  rospy.get_rostime()
        self.hack_odom_file.write("{},{},{},{},{},{},{},{}\n".format(
            now.secs,now.nsecs,
            recv_cmd.linear.x,recv_cmd.linear.y,recv_cmd.linear.z,
            recv_cmd.angular.x,recv_cmd.angular.y,recv_cmd.angular.z))


    def get_imu(self, recv_imu):
        """rosmsg show sensor_msgs/Imu
            std_msgs/Header header
              uint32 seq
              time stamp
              string frame_id
            geometry_msgs/Quaternion orientation
              float64 x
              float64 y
              float64 z
              float64 w
            float64[9] orientation_covariance
            geometry_msgs/Vector3 angular_velocity
              float64 x
              float64 y
              float64 z
            float64[9] angular_velocity_covariance
            geometry_msgs/Vector3 linear_acceleration
              float64 x
              float64 y
              float64 z
            float64[9] linear_acceleration_covariance
        """
        rospy.logdebug("Get imu")
        self.imu_file.write(
            "{},{},{},{},{},{},{},{},{},{},{},{}\n".format(
            recv_imu.header.seq,recv_imu.header.stamp.secs,
            recv_imu.header.stamp.nsecs,
            recv_imu.orientation.x,recv_imu.orientation.y,recv_imu.orientation.z,
            recv_imu.angular_velocity.x,recv_imu.angular_velocity.y,
            recv_imu.angular_velocity.z,
            recv_imu.linear_acceleration.x,recv_imu.linear_acceleration.y,
            recv_imu.linear_acceleration.z,
            )
        )

    def data_collection(self):
        rate = rospy.Rate(10)
        while not rospy.is_shutdown():
            rate.sleep()
        self.cmd_vel_file.close()
        self.imu_file.close()
        self.odom_file.close()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--scenario', type=str,
        help='Scenario of running robot. Default=pentagon',
        default='pentagon')
    parser.add_argument('--mode', type=str,
        help='Mode of collecting data, either normal (default) or attack.',
        default='normal')
    args = parser.parse_args()

    rospy.init_node('turtlebot3_data_collection')
    rospy.loginfo("Start turtlebot3_data_collection_node")

    try:
        dataCollection = DataCollection(args)
    except rospy.ROSInterruptException:
        pass

if __name__ == '__main__':
    main()
